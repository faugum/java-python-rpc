package com.python.listener.Exception;

public class CustomException extends RuntimeException{

    public CustomException(String message) {
        super(message);
    }
}
