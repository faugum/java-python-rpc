package com.python.listener.entity;

import java.util.Map;
import java.util.Objects;

/**
 * 返回值实体
 *
 * @author zt
 */
public class RequestEntity {

    private String funcName;
    private Map<String, Object> params;

    @Override
    public String toString() {
        return "RequestEntity{" +
                "funcName='" + funcName + '\'' +
                ", params=" + params +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestEntity that = (RequestEntity) o;
        return Objects.equals(funcName, that.funcName) &&
                Objects.equals(params, that.params);
    }

    @Override
    public int hashCode() {
        return Objects.hash(funcName, params);
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
}
