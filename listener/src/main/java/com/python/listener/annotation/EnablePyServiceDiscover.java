package com.python.listener.annotation;

import java.lang.annotation.*;

/**
 * 开启python服务的rpc, 扫描调用python的接口, 作用于spring-boot启动类
 *
 * @author zt
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface EnablePyServiceDiscover {

    /**
     * python 服务地址
     */
    String serviceAddress();

    /**
     * python 端口地址
     */
    int servicePort();

}
