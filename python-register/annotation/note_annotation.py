def note_annotation(**info):
    def decorate(fn):
        for item in info.items():
            fn.__annotations__[item[0]] = item[1]
            return fn
    return decorate
