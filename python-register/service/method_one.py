from annotation.note_annotation import note_annotation
from common.result_func import return_success


@note_annotation(note="测试方法11")
def m11(m11_p1):
    return return_success("请求成功", m11_p1)


@note_annotation(note="测试方法12")
def m12(m12_p1, m12_p2):
    print(123)
    return return_success("请求成功", m12_p1 + "----" + m12_p2)
