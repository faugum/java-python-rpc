import importlib
import os
import sys


def register_all_method():
    current_path = os.path.dirname(os.path.dirname(__file__))
    service_dir_path = current_path + os.path.sep + 'service'
    # 获取全部python方法文件路径
    file_paths = find_all_file_path(service_dir_path)
    # 加载全部模块并获取全部方法与对应参数信息
    return find_func_dic_list(file_paths)


def find_all_file_path(service_dir_path):
    file_paths = []
    for file in os.listdir(service_dir_path):
        if not (file.startswith("__") and file.endswith("__")):
            file_paths.append(file)
    return file_paths


def find_func_dic_list(file_paths):
    func_dic_list = []
    for file_path in file_paths:
        file_name = file_path[0:len(file_path) - 3]
        module = importlib.import_module("service." + file_name)
        for func_name in [item for item in dir(module) if
                          not (item.startswith("__") and item.endswith("__") or item.startswith("return_") or item == 'note_annotation')]:
            func_dic = {}
            # 方法名
            func_dic.setdefault('module_name', file_name + '_4_' + func_name)
            func_info = getattr(module, func_name)
            params = func_info.__code__.co_varnames
            # 参数名
            func_dic.setdefault('params', params)
            # 方法上方注解内容
            note_anno = func_info.__annotations__
            note_msg = ""
            if 'note' in note_anno:
                note_msg = note_anno['note']
            func_dic.setdefault('note', note_msg)
            func_dic_list.append(func_dic)
    return func_dic_list
