def return_success(msg, data):
    return result_info(0, msg, data)


def return_error(msg, data):
    return result_info(1, msg, data)


# 返回值
class result_info:
    code = 0
    msg = "请求成功"
    data = {}

    def __init__(self, code, msg, data):
        self.code = code
        self.msg = msg
        self.data = data


# 请求对象
class request_info_obj:
    func_name = ''
    params = {}
