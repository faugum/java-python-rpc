# java-python RPC

#### 介绍
通过简单的配置, 实现java编译阶段生成相关python方法, 并可直接调用或委派给spring, 便于java工作与python工作分离

#### 软件架构
基于jdk11和python38开发环境, 利用java编译器实现编译阶段的python方法发现,继而实现java工作与python工作解耦
java与python之间采用的socketchannel的异步tcp形式建立连接, python作为服务方定义方法, java作为调用和实现方.
总体流程为, python建立socket服务端连接, 并通过反射实现python方法的发现及缓存预热, java则重写编译器方法来实现python方法发现


#### 使用说明

1.  启动python的main.py的socket监听, python算法均要放在service包写且不要有包堆叠
2.  springbooot项目依赖listener模块, 在启动类上添加@EnablePyServiceDiscover注解并声明python的ip和端口, 编译后装配IPyRpcService 接口及可发现全部python方法信息
3,  @EnablePyServiceDiscover会将IPyRpcService的实现bean委派给springboot项目, 所以验证逻辑会判断是否和@SpringBootApplication同时注解一个启动类, 否则编译会失败
4,  java 请求参数和响应参数, 及python请求参数和相应参数已经分别封装为实体类,并通过各自的json工具类在内部进行转化
